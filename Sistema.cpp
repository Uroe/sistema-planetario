#include "Sistema.h"

Sistema::Sistema()
{
	conta=0;
	contaP=0;
}

void Sistema::calcularCoor()
{
	for(int i=0;i<10;i++)
	{
		planeta[i].setCxer((i*50)+75);
		planeta[i].setCyer((i*25)+35);
	}
}

void Sistema::CalcularCoorGiro()
{
	for(int i=0;i<10;i++)
	{
		double pi=3.14159265359;
		int xg=0;
		int yg=0;
		
		for(int j=0;j<360;j++)
			{
			
				xg=planeta[i].getCxer()*sin(((i+1)*2)*j*pi/180);
				yg=planeta[i].getCyer()*cos(((i+1)*2)*j*pi/180);
		
				planeta[i].setCxg(xg,j);
				planeta[i].setCyg(yg,j);
			}
	}	
}

void Sistema::solicitarDatosP()
{
	bool fin=false;
	string opcion;
	
	int ubi;
	char nomP[10];
	int col;
	int tam;
	do
	{
		cout<<"****Bienviniado al Creador de Sistemas****"<<endl;
		cout<<"\nEn que posicion desea ubicar el planeta seleccione un numero del 1 al 10:"<<endl;
		cout<<"Ubicacion: ";
		cin>>ubi;
		cout<<"\nNombre del Planeta que esta creando:"<<endl;
		cout<<"Nombre: ";
		cin>>nomP;
		cout<<"\nColor del Planeta:"<<endl;
		cout<<"Color: ";
		cin>>col;
		cout<<"\nTama�o del Planeta:"<<endl;
		cout<<"Tama�o: ";
		cin>>tam;
		
		ubi=ubi-1;
		planeta[ubi].setNombre(nomP);
		planeta[ubi].setColor(col);
		planeta[ubi].setTamano(tam);
		planeta[ubi].setEstCreacion(true);
		
		cout<<"\nDesea crear otro Planeta (s/n): ";
	        cin>>opcion;
	        if (opcion=="s")
	        {
	            fin=false;
	        }
	        if(opcion=="n")
	        {
	            fin=true;
	        }
	}while(fin==false);
	system("cls");

}
void Sistema::prueba()
{
	int ubi;
	cout<<"Ubicacion: ";
	cin>>ubi;
	ubi=ubi-1;
	
	cout<<"Nombre: "<<planeta[ubi].getNombre()<<endl;
	cout<<"Color: "<<planeta[ubi].getColor()<<endl;
	cout<<"Tama�o: "<<planeta[ubi].getTamano()<<endl;
	
}

void Sistema::CrearPlaneta()
{
	setbkcolor(0);
	cleardevice();
//	planeta[0].setEstCreacion(true);
//	planeta[1].setEstCreacion(true);
//	planeta[2].setEstCreacion(true);
//	planeta[3].setEstCreacion(true);
//	planeta[4].setEstCreacion(true);
//	planeta[5].setEstCreacion(true);
//	planeta[6].setEstCreacion(true);
//	planeta[7].setEstCreacion(true);
//	planeta[8].setEstCreacion(true);
//	planeta[9].setEstCreacion(true);
	
	int cx=0,cy=0;
	srand(time(NULL));
	
	for(int i=0;i<800;i++)
	{
		cx=rand()%1361;
		cy=rand()%706;
		putpixel(cx,cy,15);
	}
	
	for(int i=0;i<360;i++)
	{
//	while(true)
//	{

		for(int j=0;j<800;j++)
		{
			cx=rand()%1361;
			cy=rand()%706;
			putpixel(cx,cy,15);
		}
		
		if(planeta[0].getEstCreacion()==true)
		{
			setcolor(9);
			setlinestyle(1,0,2);
			ellipse(planeta[0].getCx(),planeta[0].getCy(),0,360,planeta[0].getCxer(),planeta[0].getCyer());
			
			setcolor(planeta[0].getColor());
			setfillstyle(SOLID_FILL,planeta[0].getColor());
			fillellipse(planeta[0].getCx()+planeta[0].getCxg(i),planeta[0].getCy()-planeta[0].getCyg(i),planeta[0].getTamano(),planeta[0].getTamano());
			
			settextstyle(SANS_SERIF_FONT,HORIZ_DIR,1);
			setbkcolor(0);//color de fondo de las letras
			setcolor(15);//Color de las letras de los botones
			outtextxy((planeta[0].getCx()-(textwidth(planeta[0].getNombre())/2))+planeta[0].getCxg(i),planeta[0].getCy()+planeta[0].getTamano()-planeta[0].getCyg(i),planeta[0].getNombre());
		}
		
//		if(planeta[1].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[1].getCx(),planeta[1].getCy(),0,360,planeta[1].getCxer(),planeta[1].getCyer());
//			
//			setcolor(planeta[1].getColor());
//			setfillstyle(SOLID_FILL,planeta[1].getColor());
//			fillellipse(planeta[1].getCx()+planeta[1].getCxg(i),planeta[1].getCy()-planeta[1].getCyg(i),planeta[1].getTamano(),planeta[1].getTamano());
//		}
//		
//		if(planeta[2].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[2].getCx(),planeta[2].getCy(),0,360,planeta[2].getCxer(),planeta[2].getCyer());
//			
//			setcolor(planeta[2].getColor());
//			setfillstyle(SOLID_FILL,planeta[2].getColor());
//			fillellipse(planeta[2].getCx()+planeta[2].getCxg(i),planeta[2].getCy()-planeta[2].getCyg(i),planeta[2].getTamano(),planeta[2].getTamano());
//		}
//		
//		if(planeta[3].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[3].getCx(),planeta[3].getCy(),0,360,planeta[3].getCxer(),planeta[3].getCyer());
//			
//			setcolor(planeta[3].getColor());
//			setfillstyle(SOLID_FILL,planeta[3].getColor());
//			fillellipse(planeta[3].getCx()+planeta[3].getCxg(i),planeta[3].getCy()-planeta[3].getCyg(i),planeta[3].getTamano(),planeta[3].getTamano());
//		}
//		
//		if(planeta[4].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[4].getCx(),planeta[4].getCy(),0,360,planeta[4].getCxer(),planeta[4].getCyer());
//			
//			setcolor(planeta[4].getColor());
//			setfillstyle(SOLID_FILL,planeta[4].getColor());
//			fillellipse(planeta[4].getCx()+planeta[4].getCxg(i),planeta[4].getCy()-planeta[4].getCyg(i),planeta[4].getTamano(),planeta[4].getTamano());
//		}
//		
//		if(planeta[5].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[5].getCx(),planeta[5].getCy(),0,360,planeta[5].getCxer(),planeta[5].getCyer());
//			
//			setcolor(planeta[5].getColor());
//			setfillstyle(SOLID_FILL,planeta[5].getColor());
//			fillellipse(planeta[5].getCx()+planeta[5].getCxg(i),planeta[5].getCy()-planeta[5].getCyg(i),planeta[5].getTamano(),planeta[5].getTamano());
//		}
//		
//		if(planeta[6].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[6].getCx(),planeta[6].getCy(),0,360,planeta[6].getCxer(),planeta[6].getCyer());
//			
//			setcolor(planeta[6].getColor());
//			setfillstyle(SOLID_FILL,planeta[6].getColor());
//			fillellipse(planeta[6].getCx()+planeta[6].getCxg(i),planeta[6].getCy()-planeta[6].getCyg(i),planeta[6].getTamano(),planeta[6].getTamano());
//		}
//		
//		if(planeta[7].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[7].getCx(),planeta[7].getCy(),0,360,planeta[7].getCxer(),planeta[7].getCyer());
//			
//			setcolor(planeta[7].getColor());
//			setfillstyle(SOLID_FILL,planeta[7].getColor());
//			fillellipse(planeta[7].getCx()+planeta[7].getCxg(i),planeta[7].getCy()-planeta[7].getCyg(i),planeta[7].getTamano(),planeta[7].getTamano());
//		}
//		
//		if(planeta[8].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[8].getCx(),planeta[8].getCy(),0,360,planeta[8].getCxer(),planeta[8].getCyer());
//			
//			setcolor(planeta[8].getColor());
//			setfillstyle(SOLID_FILL,planeta[8].getColor());
//			fillellipse(planeta[8].getCx()+planeta[8].getCxg(i),planeta[8].getCy()-planeta[8].getCyg(i),planeta[8].getTamano(),planeta[8].getTamano());
//		}
//		
//		if(planeta[9].getEstCreacion()==true)
//		{
//			setcolor(9);
//			setlinestyle(1,0,2);
//			ellipse(planeta[9].getCx(),planeta[9].getCy(),0,360,planeta[9].getCxer(),planeta[9].getCyer());
//			
//			setcolor(planeta[9].getColor());
//			setfillstyle(SOLID_FILL,planeta[9].getColor());
//			fillellipse(planeta[9].getCx()+planeta[9].getCxg(i),planeta[9].getCy()-planeta[9].getCyg(i),planeta[9].getTamano(),planeta[9].getTamano());
//		}
		
//		if(conta==359)
//		{
//			conta=0;
//		}
		delay(75);
		cleardevice();
//		conta++;
	}
}
