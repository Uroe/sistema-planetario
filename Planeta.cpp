#include "Planeta.h"

Planeta::Planeta()
{
//	nomP='';
	colP=2;
	tamano=10;
	cx=680;
	cy=352;
	cxer=0;
	cyer=0;
	estCreacion=false;
}

void Planeta::setNombre(char *nom)
{
	strcpy(nomP,nom);
}

void Planeta::setColor(int col)
{
	colP=col;
}

void Planeta::setTamano(int tam)
{
	tamano=tam;
}

void Planeta::setCx(int cx)
{
	cx=cx;
}

void Planeta::setCy(int cy)
{
	cy=cy;
}

void Planeta::setCxer(int xer)
{
	cxer=xer;
}

void Planeta::setCyer(int yer)
{
	cyer=yer;
}

void Planeta::setCxg(int v,int n)
{
	cxg[n]=v;
}

void Planeta::setCyg(int v,int n)
{
	cyg[n]=v;
}

void Planeta::setEstCreacion(bool estC)
{
	estCreacion=estC;
}

char *Planeta::getNombre()
{
	return nomP;
}

int Planeta::getColor()
{
	return colP;	
}

int Planeta::getTamano()
{
	return tamano;
}

int Planeta::getCx()
{
	return cx;	
}

int Planeta::getCy()
{
	return cy;
}

int Planeta::getCxer()
{
	return cxer;
}

int Planeta::getCyer()
{
	return cyer;
}

int Planeta::getCxg(int n)
{
	return cxg[n];
}

int Planeta::getCyg(int n)
{
	return cyg[n];
}

bool Planeta::getEstCreacion()
{
	return estCreacion;
}
