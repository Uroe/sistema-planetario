#ifndef SISTEMA_H
#define SISTEMA_H
#include "Planeta.h"

class Sistema
{
	private:
		string nomE;
		int colE;
		Planeta planeta[10];
		int conta;
		int contaP;
	public:
		Sistema();
		void calcularCoor();
		void CalcularCoorGiro();
		void solicitarDatosE();
		void solicitarDatosP();
		void CrearPlaneta();
		void prueba();
		
};

#endif
