#ifndef PLANETA_H
#define PLANETA_H
#include<graphics.h>
#include<conio.h>
#include<dos.h>
#include<iostream>
#include<cmath>
#include<string.h>
#include<stdlib.h>
#include<time.h>

using namespace std;

class Planeta
{
	private:
		char nomP[10];
		int colP;
		int tamano;
		int cx;
		int cy;
		int cxer;
		int cyer;
		int cxg[360];
		int cyg[360];
		bool estCreacion;
		
	public:
		Planeta();
		void setNombre(char *nom);
		void setColor(int col);
		void setTamano(int tam);	
		void setCx(int cx);
		void setCy(int cy);
		void setCxer(int xer);
		void setCyer(int yer);
		void setCxg(int n,int v);
		void setCyg(int n,int v);
		void setEstCreacion(bool estC);
		char* getNombre();
		int getColor();
		int getTamano();
		int getCx();
		int getCy();
		int getCxer();
		int getCyer();
		int getCxg(int n);
		int getCyg(int n);
		bool getEstCreacion();
		
};

#endif
