#include "Menu.h"

void Menu::menuPrincipal()
{
	setbkcolor(0);
	cleardevice();
	int x=0,y=0;
	float pi=3.14159265359;
	int cx=0,cy=0;
	srand(time(NULL));
	
	for(int i=0;i<1000;i++)
	{
		cx=rand()%1361;
		cy=rand()%706;
		putpixel(cx,cy,15);
	}
		
	settextstyle(SANS_SERIF_FONT,HORIZ_DIR,8);
	setbkcolor(0);
	setcolor(15);
	outtextxy(680-(textwidth((char*)"Sistemas Planetarios")/2),80-(textheight((char*)"Sistemas Planetarios")/2),(char*)"Sistemas Planetarios");
	
	for(int j=0;j<7;j++)
	{
		setcolor(14);
		setfillstyle(SOLID_FILL,14);
		fillellipse(680,326,25,25);
		
		setcolor(9);
		setlinestyle(1,0,2);
		ellipse(680,326,0,360,(j*50)+50,(j*25)+30);
		
		x=((j*50)+50)*sin((j*135)*pi/180);
		y=((j*25)+30)*cos((j*135)*pi/180);
		
		setcolor(2+j);
		setfillstyle(SOLID_FILL,2+j);
		fillellipse(680+x,326-y,10+(j*2),10+(j*2));
	}
		
	setfillstyle(SOLID_FILL,15);
	setcolor(15);
	setlinestyle(1,0,2);	
	rectangle(359,554,561,644);
	rectangle(579,554,781,644);	
	rectangle(799,554,1001,644);
	
	settextstyle(SANS_SERIF_FONT,HORIZ_DIR,1);
	setbkcolor(0);
	setcolor(15);
	outtextxy(460-(textwidth((char*)"Nuevos Sistemas")/2),598-(textheight((char*)"Nuevos Sistemas")/2),(char*)"Nuevos Sistemas");
	outtextxy(680-(textwidth((char*)"Editar y Eliminar")/2),598-(textheight((char*)"Editar y Eliminar")/2),(char*)"Editar y Eliminar");
	outtextxy(900-(textwidth((char*)"Salir")/2),598-(textheight((char*)"Salir")/2),(char*)"Salir");

}

void Menu::menuNuevoSistema()
{
	setbkcolor(0);
	cleardevice();
	
	int cx=0,cy=0;
	srand(time(NULL));
	
	for(int i=0;i<1000;i++)
	{
		cx=rand()%1361;
		cy=rand()%706;
		setlinestyle(1,0,2);
		putpixel(cx,cy,15);
	}
	
	setfillstyle(SOLID_FILL,15);
	setcolor(15);
	setlinestyle(1,0,2);	
	rectangle(50,50,250,140);
	rectangle(300,75,1200,300);
	rectangle(360,400,560,490);
	rectangle(580,400,780,490);	
	rectangle(800,400,1000,490);
	rectangle(470,510,670,600);
	rectangle(690,510,890,600);	
	
	
	settextstyle(SANS_SERIF_FONT,HORIZ_DIR,1);
	setbkcolor(0);
	setcolor(15);
	outtextxy(150-(textwidth((char*)"Nuevos Sistemas")/2),95-(textheight((char*)"Nuevos Sistemas")/2),(char*)"Nuevos Sistemas");
	outtextxy(460-(textwidth((char*)"Sistema 1")/2),445-(textheight((char*)"Sistema 1")/2),(char*)"Sistema 1");
	outtextxy(680-(textwidth((char*)"Sistema 2")/2),445-(textheight((char*)"Sistema 2")/2),(char*)"Sistema 2");
	outtextxy(900-(textwidth((char*)"Sistema 3")/2),445-(textheight((char*)"Sistema 3")/2),(char*)"Sistema 3");
	outtextxy(570-(textwidth((char*)"Mostrar Sistema(s)")/2),555-(textheight((char*)"Mostrar Sistema(s)")/2),(char*)"Mostrar Sistema(s)");
	outtextxy(790-(textwidth((char*)"Volver al Menu")/2),555-(textheight((char*)"Volver al Menu")/2),(char*)"Volver al Menu");
	
}

void Menu::menuEditarEliminar()
{
	setbkcolor(0);
	cleardevice();
	
	int cx=0,cy=0;
	srand(time(NULL));
	
	for(int i=0;i<1000;i++)
	{
		cx=rand()%1361;
		cy=rand()%706;
		setlinestyle(1,0,2);
		putpixel(cx,cy,15);
	}
	
	setfillstyle(SOLID_FILL,15);
	setcolor(15);
	setlinestyle(1,0,2);	
	rectangle(50,50,250,140);
	rectangle(300,75,1200,300);
	rectangle(360,400,560,490);
	rectangle(580,400,780,490);	
	rectangle(800,400,1000,490);
	rectangle(470,510,670,600);
	rectangle(690,510,890,600);	
	
	
	settextstyle(SANS_SERIF_FONT,HORIZ_DIR,1);
	setbkcolor(0);
	setcolor(15);
	outtextxy(150-(textwidth((char*)"Editar y Eliminar")/2),95-(textheight((char*)"Editar y Eliminar")/2),(char*)"Editar y Eliminar");
	outtextxy(460-(textwidth((char*)"Sistema 1")/2),445-(textheight((char*)"Sistema 1")/2),(char*)"Sistema 1");
	outtextxy(680-(textwidth((char*)"Sistema 2")/2),445-(textheight((char*)"Sistema 2")/2),(char*)"Sistema 2");
	outtextxy(900-(textwidth((char*)"Sistema 3")/2),445-(textheight((char*)"Sistema 3")/2),(char*)"Sistema 3");
	outtextxy(570-(textwidth((char*)"Mostrar Sistema(s)")/2),555-(textheight((char*)"Mostrar Sistema(s)")/2),(char*)"Mostrar Sistema(s)");
	outtextxy(790-(textwidth((char*)"Volver al Menu")/2),555-(textheight((char*)"Volver al Menu")/2),(char*)"Volver al Menu");
	
}
